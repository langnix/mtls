package de.harm.test.mtls.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@Slf4j
public class MtlsController {
    @GetMapping("/public")
    public String getData() {

        log.info("getData..Public");
        return "Public data\n";
    }

    @GetMapping("/protected")
    public String getDataProtected(Principal principal) {

        log.info("getData..protected");
        return "Private Data of:" + principal.getName();
    }
}
